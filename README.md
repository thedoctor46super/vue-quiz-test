# vue-quiz-test

> Simple SSR Quiz app to fetch some questions, display them to the user and eventually show the results.
> Results are persisted to Mongo DB.

## Run the server locally

``` bash
# install server dependencies
$ cd server
$ npm install

# serve with hot reload at localhost:8081
$ npm start
```

## Run the client locally

``` bash
# install client dependencies
$ cd client
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```
