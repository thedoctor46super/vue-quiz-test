export default {
  listener: null,
  destroy() {
    window.removeEventListener('scroll', this.listener)
  },
  watch(selector) {
    this.listener = window.addEventListener('scroll', () => {
      const scrolled = window.scrollY

      document.querySelectorAll(selector).forEach((element, index) => {
        const initY = element.offsetTop
        const height = element.clientHeight
        const endY = initY + height

        // Check if element is in viewport
        const rect = element.getBoundingClientRect()
        const visible =
          (rect.height > 0 || rect.width > 0) &&
          rect.bottom >= 0 &&
          rect.right >= 0 &&
          rect.top <=
            (window.innerHeight || document.documentElement.clientHeight) &&
          rect.left <=
            (window.innerWidth || document.documentElement.clientWidth)

        if (visible) {
          const diff = scrolled - initY
          const ratio = Math.round((diff / height) * 100)
          element.style.backgroundPosition =
            'center ' + parseInt(-(ratio * 2)) + 'px'
        }
      })
    })
  }
}
