import Api from './Api'

export default {
  fetchQuiz() {
    return Api().get('quiz')
  },
  fetchResults() {
    return Api().get('results')
  },
  addResult(params) {
    return Api().post('add_result', params)
  }
}
