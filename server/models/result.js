var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ResultSchema = new Schema({
  user: String,
  result: Number
});

var Result = mongoose.model("Result", ResultSchema);
module.exports = Result;
