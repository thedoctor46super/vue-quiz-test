var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var QuizSchema = new Schema({
  title: String,
  questions: [{
    question: String,
    options: [{
      type: String
    }],
    answer: String
  }]
});

var Quiz = mongoose.model("Quiz", QuizSchema);
module.exports = Quiz;
