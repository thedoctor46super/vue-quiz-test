const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

const mongodb_conn_module = require('./mongodbConnModule');
var db = mongodb_conn_module.connect();

var Result = require("../models/result");
var Quiz = require("../models/quiz");

app.get('/quiz', (req, res) => {
	var quizId = '5beec775eabb348ba3c41a65';
	var query = Quiz.findById(quizId, null, function(error, quiz) {
		if (error) { console.error(error); }

	  res.send({
			quiz: {
				title: quiz.title,
				questions: quiz.questions
			}
		})
	});
})

app.get('/results', (req, res) => {
	var query = Result.find({}, 'user result', function(error, results) {
		if (error) { console.error(error); }

	  res.send({
			results: results
		})
	});
})

app.post('/add_result', (req, res) => {
	var user = req.body.user;
	var result = req.body.result;
	var new_result = new Result({
		user: user,
		result: result
	});

	new_result.save(function (error) {
		if (error) {
			console.log(error)
		}
		res.send({
			success: true
		})
	});
})

app.listen(process.env.PORT || 8081)
