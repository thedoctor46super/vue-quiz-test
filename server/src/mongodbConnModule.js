var mongoose = require('mongoose');

module.exports.connect = function() {
	mongoose.connect('mongodb://vue-quiz-test:vue-quiz-test1@ds261253.mlab.com:61253/vue-quiz-test', { useNewUrlParser: true });
	var db = mongoose.connection;
	db.on("error", console.error.bind(console, "connection error"));
	db.once("open", function(callback){
	  console.log("Connection Succeeded");
	});
	return db;
}